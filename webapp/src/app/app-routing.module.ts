// The MIT License (MIT)
//
// Copyright (c) 2018 Cranky Kernel
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {TradeComponent} from "./trade/trade.component";
import {ConfigComponent} from "./config/config.component";
import {HistoryComponent} from './history/history.component';
import {TradeDetailComponent} from './trade-detail/trade-detail.component';
import {LoginComponent} from "./login/login.component";
import {CompositeGuard} from "./composite.guard";
import {BinanceBalancesComponent} from "./binance-balances/binance-balances.component";

const routes: Routes = [
    {
        path: "trade",
        component: TradeComponent,
        canActivate: [
            CompositeGuard,
        ],
        data: {
            authRequired: true,
            configRequired: true,
        }
    },
    {
        path: "trade/:tradeId",
        component: TradeDetailComponent,
        canActivate: [
            CompositeGuard,
        ],
        data: {
            authRequired: true,
            configRequired: true,
        }
    },
    {
        path: "config",
        component: ConfigComponent,
        canActivate: [
            CompositeGuard,
        ],
        data: {
            authRequired: true,
            configRequired: false,
        }
    },
    {
        path: "history",
        component: HistoryComponent,
        canActivate: [
            CompositeGuard,
        ],
        data: {
            authRequired: true,
            configRequired: true,
        }
    },
    {
        path: "binance/balances",
        component: BinanceBalancesComponent,
        canActivate: [
            CompositeGuard,
        ],
        data: {
            authRequired: true,
            configRequired: true,
        }
    },
    {
        path: "login",
        pathMatch: "full",
        component: LoginComponent,
    },
    {
        path: "",
        pathMatch: "full",
        redirectTo: "trade",
    },
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes),
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {
}
