// The MIT License (MIT)
//
// Copyright (c) 2018 Cranky Kernel
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";

import {AppRoutingModule} from "./app-routing.module";
import {AppComponent} from "./app.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {BinanceService} from "./binance.service";
import {ToastrService} from "./toastr.service";
import {StopLossFormComponent} from "./stoplossform/stop-loss-form.component";
import {WithQuoteAssetPipe} from "./pipes/withquoteasset.pipe";
import {TradeTableComponent} from "./trade-table/trade-table.component";
import {BinanceApiService} from "./binance-api.service";
import {TradeComponent} from "./trade/trade.component";
import {ConfigComponent} from "./config/config.component";

import * as fontawesome from "@fortawesome/fontawesome";
import * as faCog from "@fortawesome/fontawesome-free-solid/faCog";
import * as faQuestion from "@fortawesome/fontawesome-free-solid/faQuestion";
import * as faSyncAlt from "@fortawesome/fontawesome-free-solid/faSyncAlt";
import {HttpClientModule} from "@angular/common/http";
import {HistoryComponent} from './history/history.component';

import {TradeDetailComponent} from './trade-detail/trade-detail.component';
import {TradeTableRowComponent} from './trade-table-row/trade-table-row.component';
import {TrailingProfitFormComponent} from './trailingprofitform/trailing-profit-form.component';
import {AboutComponent} from './about/about.component';
import {LoginComponent} from './login/login.component';
import { BinanceBalancesComponent } from './binance-balances/binance-balances.component';

fontawesome.library.add(faCog);
fontawesome.library.add(faQuestion);
fontawesome.library.add(faSyncAlt);

@NgModule({
    declarations: [
        AppComponent,
        TradeComponent,
        TrailingProfitFormComponent,
        StopLossFormComponent,
        WithQuoteAssetPipe,
        TradeTableComponent,
        TradeTableRowComponent,
        ConfigComponent,
        HistoryComponent,
        TradeDetailComponent,
        AboutComponent,
        LoginComponent,
        BinanceBalancesComponent,
    ],
    imports: [
        // Angular modules.
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        HttpClientModule,
    ],
    providers: [
        BinanceService,
        ToastrService,
        BinanceApiService,
    ],
    bootstrap: [
        AppComponent,
    ]
})
export class AppModule {
}
