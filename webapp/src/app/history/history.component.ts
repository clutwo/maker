// The MIT License (MIT)
//
// Copyright (c) 2018 Cranky Kernel
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import {Component, OnInit} from '@angular/core';
import {TradeState} from '../maker.service';
import {toAppTradeState} from '../trade-table/trade-table.component';
import {MakerApiService} from "../maker-api.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
    selector: 'app-history',
    templateUrl: './history.component.html',
    styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {

    trades: TradeState[] = [];

    page: number = 0;

    pageSize = 50;

    constructor(private makerApi: MakerApiService,
                private route: ActivatedRoute,
                private router: Router) {
    }

    ngOnInit() {
        this.route.params.subscribe((params) => {
            console.log(params);
            if (params.page !== undefined) {
                this.page = +params.page;
            }
            this.refresh()
        });
    }

    refresh() {
        let offset = this.page * this.pageSize;
        console.log(offset);
        this.makerApi.get("/api/trade/query", {
            params: {
                offset: offset,
                limit: this.pageSize,
            }
        }).subscribe((trades: TradeState[]) => {
            this.trades = trades
                .map((trade) => {
                    return toAppTradeState(trade);
                });
        });
    }

    gotoPreviousPage() {
        let page = this.page < 1 ? 0 : this.page - 1;
        let params = {
            page: page,
        };
        this.router.navigate([".", params], {
            queryParamsHandling: "merge",
        });
    }

    gotoNextPage() {
        let params = {
            page: this.page + 1,
        };
        this.router.navigate([".", params], {
            queryParamsHandling: "merge",
        });
    }
}
