// The MIT License (MIT)
//
// Copyright (c) 2018 Cranky Kernel
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import {Injectable} from "@angular/core";
import {BinanceApiService, buildTickerFromStream, ExchangeInfo, PriceTicker, SymbolInfo} from "./binance-api.service";
import {map, multicast, refCount, take, tap} from "rxjs/operators";
import {Observable, Subject} from "rxjs";
import {ReplaySubject} from "rxjs/ReplaySubject";
import {MakerService} from "./maker.service";
import {MakerApiService} from "./maker-api.service";
import {ToastrService} from "./toastr.service";
import {LoginService} from "./login.service";
import {BinanceProxyService} from "./binance-proxy.service";

/**
 * Enum of types that can be used as a price source.
 */
export enum PriceSource {
    /** The price of the last trade is used. */
    LAST_PRICE = "LAST_PRICE",

    /** The best bid is used. Queries the REST API. */
    BEST_BID = "BEST_BID",

    /** The best ask is used. Queries the REST API. */
    BEST_ASK = "BEST_ASK",

    MANUAL = "MANUAL",
}

export enum LimitSellType {
    PERCENT = "PERCENT",
    PRICE = "PRICE",
}

export interface SymbolMap {
    [key: string]: SymbolInfo;
}

@Injectable()
export class BinanceService {

    /** A list of all symbols (pairs). */
    symbols: string[] = [];

    /** A list of all quote assets: BTC, BNB, etc. */
    quoteAssets: string[] = [];

    /**
     * SymbolInfo objects keyed by symbol.
     */
    symbolMap: SymbolMap = {};

    streams$: { [key: string]: Observable<any> } = {};

    private isReadySubject: Subject<boolean> = null;

    public isReady$: Observable<boolean> = null;

    constructor(private api: BinanceApiService,
                private maker: MakerService,
                private makerApi: MakerApiService,
                private toastr: ToastrService,
                private loginService: LoginService,
                private binanceProxy: BinanceProxyService,
    ) {
        this.isReadySubject = new ReplaySubject<boolean>(1);
        this.isReady$ = this.isReadySubject.pipe(take(1));
        this.loginService.$onLogin.asObservable().pipe(take(1))
            .subscribe(() => {
                this.init();
            });
    }

    private init() {
        // Get config then do initialization that depends on config.
        this.makerApi.getConfig().subscribe(() => {
            this.updateExchangeInfo().subscribe(() => {
                this.isReadySubject.next(true);
            });
        });
    }

    subscribeToTicker(symbol: string): Observable<PriceTicker> {
        const stream = `${symbol.toLowerCase()}@ticker`;
        if (!this.streams$[stream]) {
            const path = `/ws/${stream}`;
            this.streams$[stream] = this.api.openStream(path)
                .pipe(
                    multicast(new Subject<any>()),
                    refCount(),
                    map((ticker) => {
                        return buildTickerFromStream(ticker);
                    })
                );
        }
        return this.streams$[stream];
    }

    updateExchangeInfo(): Observable<ExchangeInfo> {
        return this.binanceProxy.getExchangeInfo().pipe(tap((exchangeInfo) => {
            const quoteAssets: any = {};

            exchangeInfo.symbols.forEach((symbol) => {
                this.symbolMap[symbol.symbol] = symbol;
                quoteAssets[symbol.quoteAsset] = true;
            });

            this.symbols = Object.keys(this.symbolMap).filter((key) => {
                if (this.symbolMap[key].status !== "TRADING") {
                    return false;
                }
                return true;
            }).sort();

            this.quoteAssets = Object.keys(quoteAssets);
        }));
    }

}

export interface OpenTradeOptions {
    symbol: string;
    quantity: number;

    priceSource: PriceSource;
    priceAdjustment: number;
    price?: number;

    stopLossEnabled?: boolean;
    stopLossPercent?: number;

    limitSellEnabled?: boolean;
    limitSellType?: LimitSellType;
    limitSellPercent?: number;
    limitSellPrice?: number;

    trailingProfitEnabled?: boolean;
    trailingProfitPercent?: number;
    trailingProfitDeviation?: number;

    offsetTicks?: number,
}
