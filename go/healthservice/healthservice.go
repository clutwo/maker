// The MIT License (MIT)
//
// Copyright (c) 2018 Cranky Kernel
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package healthservice

import "sync"

type BinanceUserSocketState string

const (
	BINANCE_USER_SOCKET_STATE_CONNECTING        BinanceUserSocketState = "connecting"
	BINANCE_USER_SOCKET_STATE_OK                BinanceUserSocketState = "ok"
	BINANCE_USER_SOCKET_STATE_CONNECTION_FAILED BinanceUserSocketState = "connection failed"
)

type State struct {
	BinanceUserSocketState BinanceUserSocketState `json:"binanceUserSocketState"`
}

type Service struct {
	lock        sync.RWMutex
	subscribers map[chan *State]bool
	state       State
}

func New() *Service {
	return &Service{
		subscribers: make(map[chan *State]bool),
		state: State{
			BinanceUserSocketState: BINANCE_USER_SOCKET_STATE_CONNECTING,
		},
	}
}

func (s *Service) Update(state BinanceUserSocketState) {
	s.lock.Lock()
	s.state.BinanceUserSocketState = state
	s.broadcast()
	s.lock.Unlock()
}

func (s *Service) Subscribe() chan *State {
	s.lock.Lock()
	defer s.lock.Unlock()
	channel := make(chan *State, 1)
	channel <- &s.state
	s.subscribers[channel] = true
	return channel
}

func (s *Service) Unsubscribe(channel chan *State) {
	s.lock.Lock()
	defer s.lock.Unlock()
	if _, exists := s.subscribers[channel]; !exists {
		return
	}
	delete(s.subscribers, channel)
}

func (s *Service) broadcast() {
	for channel := range s.subscribers {
		select {
		case channel <- &s.state:
		default:
		}
	}
}
